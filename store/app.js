import { updateValue, updateKey, setProperty } from '~/helpers/store.helper'
import { Loader } from '~/interfaces/loader.interface'
import { Toast } from '~/interfaces/toast.interface'

const getDefaultState = () => ({
  isPosting: false,
  toast: { ...Toast },
  loader: { ...Loader }
})

export const state = () => ({
  ...getDefaultState()
})

export const mutations = {
  setProperty,
  updateValue,
  updateKey,

  resetState (state) {
    Object.assign(state, getDefaultState())
  }
}

export const actions = {
  resetAppState ({ commit }) {
    commit('resetState')
  }
}

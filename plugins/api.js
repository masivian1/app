import { loader } from '~/interfaces/loader.interface'
import { toast } from '~/interfaces/toast.interface'

export default function ({ $axios }, inject) {
  const api = $axios.create()

  api.onRequestError(() => {
    // Close loader
    setTimeout(() => loader.close(), 2000)
    // Open toast
    toast.show('error', 'Ocurrió un error inesperado')
  })

  api.onResponse(() => {
    // Close loader
    setTimeout(() => loader.close(), 2000)
  })

  api.onError(() => {
    // Close loader
    setTimeout(() => loader.close(), 2000)
    return Promise.resolve(false)
  })

  // Set baseURL to api
  api.setBaseURL('http://localhost:5000')

  // Inject to context as $api
  inject('api', api)
}

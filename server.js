const cors = require('cors')
const express = require('express')
const fetch = require('node-fetch')

const PORT = 5000
const app = express()

app.use(cors())
const corsOptions = {
  origin: '*'
}

// This function runs if the http://localhost:5000/getComic/{id} endpoint
// is requested with a GET request
app.get('/getComic/:id', cors(corsOptions), async (req, res) => {
  const requestEndpoint = `https://xkcd.com/${req.params.id}/info.0.json`

  const fetchOptions = {
    method: 'GET'
  }
  const response = await fetch(requestEndpoint, fetchOptions)
  const jsonResponse = await response.json()
  res.json(jsonResponse)
})

app.listen(PORT, () => {
  console.log(`App listening at http://localhost:${PORT}`)
})

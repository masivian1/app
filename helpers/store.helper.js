export const updateKey = (state, data) => {
  state[data.object][data.key] = data.value
}

export const updateValue = (state, data) => {
  state[data.key] = data.value
}

/**
 * SET property in module STORE
 * @param {Object} state
 * @param {Object} data
 */
export const setProperty = (state, data) => {
  const oneKey = data.oneKey
  const twoKey = data.twoKey
  const threeKey = data.threeKey
  const fourKey = data.fourKey
  const value = data.value

  /* Set FOUR key in object */
  if (oneKey && twoKey && threeKey && fourKey) {
    state[oneKey][twoKey][threeKey][fourKey] = value
    return
  }

  /* Set THREE key in object */
  if (oneKey && twoKey && threeKey) {
    state[oneKey][twoKey][threeKey] = value
    return
  }

  /* Set TWO key in object */
  if (oneKey && twoKey) {
    state[oneKey][twoKey] = value
    return
  }

  /* Set ONE key in object */
  if (oneKey) {
    state[oneKey] = value
  }
}

/**
 * Generate Model with properties in Store
 * @param {Array} props
 * @param {Object} { path, mut}
 * @return {Object}
 */
export const modelGenerator = (props = [], { path, mut } = {}) => {
  return props.reduce((obj, prop) => {
    const oneKey = prop.oneKey
    const twoKey = prop.twoKey
    const threeKey = prop.threeKey
    const fourKey = prop.fourKey
    const defaultValue = prop.defaultValue

    obj[prop.property] = {
      get () {
        if (oneKey && twoKey && threeKey && fourKey) {
          return (
            $nuxt.$store.state[path][oneKey][twoKey][threeKey] || defaultValue
          )
        }
        if (oneKey && twoKey && threeKey) {
          return (
            $nuxt.$store.state[path][oneKey][twoKey][threeKey] || defaultValue
          )
        }
        if (oneKey && twoKey) {
          return $nuxt.$store.state[path][oneKey][twoKey] || defaultValue
        }
        if (oneKey) {
          return $nuxt.$store.state[path][oneKey] || defaultValue
        }

        return null
      },
      set (value) {
        $nuxt.$store.commit(mut, {
          oneKey,
          twoKey,
          threeKey,
          fourKey,
          value
        })
      }
    }
    return obj
  }, {})
}
